package org.openhab.binding.nadreceiver.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import gnu.io.NRSerialPort;

// TODO eliminate use of RuntimeException
// TODO better error checking
// TODO should we require a mode to be set, or just assume one?

public class PortServer {
    private NRSerialPort serialPort;
    private Socket tcpPort;

    private boolean serialMode;
    private boolean tcpMode;

    // for TCP mode
    private String hostName;
    private int port;

    // for Serial mode
    private String comPort;
    private int baudRate;

    public boolean isSerialMode() {
        return serialMode;
    }

    public boolean isTcpMode() {
        return tcpMode;
    }

    public void disconnect() {
        if (serialMode) {
            serialPort.disconnect();
        } else if (tcpMode) {
            try {
                tcpPort.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            throw new RuntimeException("No mode set.");
        }
    }

    public InputStream getInputStream() throws IOException {
        if (!isConnected()) {
            throw new RuntimeException("Not Connected.");
        } else if (isTcpMode()) {
            return tcpPort.getInputStream();
        } else {
            return serialPort.getInputStream();
        }
    }

    public boolean isConnected() {
        if (serialMode) {
            return serialPort.isConnected();
        } else if (tcpMode) {
            return tcpPort.isConnected();
        } else {
            throw new RuntimeException("No mode set.");
        }
    }

    public OutputStream getOutputStream() throws IOException {
        if (!isConnected()) {
            throw new RuntimeException("Not Connected.");
        } else if (isTcpMode()) {
            return tcpPort.getOutputStream();
        } else {
            return serialPort.getOutputStream();
        }
    }

    public void connect() throws IOException {
        if (serialMode) {
            serialPort = new NRSerialPort(comPort, baudRate);
            serialPort.connect();
        } else if (tcpMode) {
            tcpPort = new Socket(hostName, port);
        } else {
            throw new RuntimeException("No mode set.");
        }
    }

    public void setSerialMode(String comPort, int baudRate) {
        if (serialPort != null) {
            throw new RuntimeException("Serial mode already set.");
        }
        if (tcpPort != null) {
            throw new RuntimeException("Mode already set to TCP.");
        }

        serialMode = true;
        tcpMode = false;

        this.comPort = comPort;
        this.baudRate = baudRate;

    }

    public void setTcpMode(String hostName, int port) {
        if (tcpPort != null) {
            throw new RuntimeException("TCP mode already set.");
        }
        if (serialPort != null) {
            throw new RuntimeException("Mode already set to Serial.");
        }

        tcpMode = true;
        serialMode = false;
        tcpPort = new Socket();
        this.hostName = hostName;
        this.port = port;
    }

}
